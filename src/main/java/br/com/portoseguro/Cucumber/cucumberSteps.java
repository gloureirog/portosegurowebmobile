package br.com.portoseguro.Cucumber;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import br.com.portoseguro.connectionecrud.ConnectionAppium;
import br.com.portoseguro.metodos.Scroll;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import junit.framework.Assert;

public class cucumberSteps {

	private WebDriver driver;

	private AndroidDriver<?> driverAndroid;
	public static String OpcaoTeste = "";

	@Before("@start")
	public void AbreDriverWeb() throws Exception {

		JInternalFrame p = new JInternalFrame();

		Object[] options = { "|Teste Web|", "|Teste Mobile|", "|Sair do Sistema|" };

		int n = JOptionPane.showOptionDialog(p, "Favor Escolha o Tipo do Teste " + "", "",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				new ImageIcon("C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/target/Indra.png"), options,
				options[2]);

		if (n == JOptionPane.YES_OPTION) {

			JOptionPane.showMessageDialog(null, "TESTE WEB SELECIONADO", null, n,
					new ImageIcon("C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/target/portoweb.png"));
			OpcaoTeste = "1";
		}
		if (n == JOptionPane.NO_OPTION) {

			JOptionPane.showMessageDialog(null, "TESTE MOBILE SELECIONADO", null, n, new ImageIcon(
					"C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/target/portomobile.png"));
			OpcaoTeste = "2";
		}
		if (n == JOptionPane.CANCEL_OPTION) {

			System.exit(0);
		}

		if (OpcaoTeste.equals("1")) {
			try {
				System.setProperty("webdriver.chrome.driver",
						"C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.manage().window().maximize();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"Ocorreu um erro no sistema, Talvez o Chrome Driver n�o foi encontrado, certifique-se que o mesmo est� no resposit�rio informado"
								+ e);
				System.exit(0);
			}

		}
		if (OpcaoTeste.equals("2")) {
			try {
				driverAndroid = ConnectionAppium.APPConfig();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"Ocorreu um erro no sistema,  o Appium e ou o Genymotion podem n�o estar abertos" + e);
				System.exit(0);
			}

		}

	}

	/*
	 * 1 - OP��O SITE PORTO SEGURO JOGAR PARA O SITE E CONFIRMA SE REALMENTE EST�
	 * NELE. 2- OP��O MOBILE N�O EST� IMPLEMENTADA
	 */

	@Given("^que eu estou com o sistema aberto$")
	public void abrirPaginaInicial() throws Throwable {
		// WEB
		if (OpcaoTeste.equals("1")) {

			driver.navigate().to("https://www.portoseguro.com.br");
			Assert.assertTrue("//*[@id=\"canais-atendimento\"]/div/div[4]/a", true);
		}
		// WEB

	}

	/*
	 * 1 - OP��O SITE PORTO SEGURO CHAT ONLINE 2- OP��O APLICATIVO PORTO SEGURO CHAT
	 */
	@When("^eu abrir o chat online$")
	public void AbrirChatOnline() throws Throwable {
		// WEB
		if (OpcaoTeste.equals("1")) {

			driver.findElement(By.xpath("//*[@id=\"canais-atendimento\"]/div/div[4]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/div[1]/div[2]/a")).click();

		}
		// WEB

		// MOBILE
		if (OpcaoTeste.equals("2")) {

			Thread.sleep(3000);
			driverAndroid.findElement(By.id("br.com.portoseguro.auto:id/text_view_treatment")).click();
			Thread.sleep(3000);

			driverAndroid.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.LinearLayout"))
					.click();
			Thread.sleep(3000);
		}
		// MOBILE

	}

	/*
	 * 1 - OP��O SITE PORTO VALIDA SE EST� NA GUIA CHAT ONLINE E MAXIMIZA A TELA 2 -
	 * OP��O MOBILE N�O EST� IMPLEMENTADA
	 */

	@Then("^deverei validar se estou no chat online$")
	public void ValidarSeEstouNoChat() throws Throwable {
		if (OpcaoTeste.equals("1")) {
			// WEB

			for (String WinHalndle : driver.getWindowHandles()) {

				driver.switchTo().window(WinHalndle);
			}

			driver.manage().window().maximize();
			String URL = driver.getCurrentUrl();
			Assert.assertEquals(URL,
					"https://wwws.portoseguro.com.br/atendimento-online/xwc/xwc.dll?f=F8001&ct=CENTRAL_24HS&sel=SitePortoSeguro");
			// WEB
		}

	}

	/*
	 * 1 - OP��O SITE PORTO SEGURO INFORMA UM NOME NO FORM DO CHAT 2- OP��O
	 * APLICATIVO PORTO SEGURO INFORMA UM NOME NO FORM DO CHAT
	 */

	@When("^informar um nome \"(.*?)\" valido$")
	public void informarNomeValido(String nome) throws Throwable {
		if (OpcaoTeste.equals("1")) {
			driver.findElement(By.name("nome")).sendKeys(nome);
		}

		if (OpcaoTeste.equals("2")) {
			Thread.sleep(3000);
			driverAndroid.findElement(By.xpath("//android.widget.EditText[@content-desc=\"Nome*\"]")).sendKeys(nome);
		}

	}

	/*
	 * 1 - OP��O SITE PORTO SEGURO INFORMA UM PERFIL NO FORM DO CHAT 2- OP��O
	 * APLICATIVO PORTO SEGURO INFORMA UM PERFIL NO FORM DO CHAT
	 */
	@And("^informar um perfil\"(.*?)\" valido$")
	public void InformarUmPerfilValido(String perfil) throws Throwable {
		if (OpcaoTeste.equals("1")) {
			// WEB
			driver.findElement(By.xpath("//*[@id=\"perfil\"]")).sendKeys(perfil);
		}

		if (OpcaoTeste.equals("2")) {
			Thread.sleep(3000);
			Scroll.scrollDown();
			Thread.sleep(3000);
			driverAndroid.findElement(By.xpath("//android.widget.Spinner[@content-desc=\"Selecione um perfil\"]"))
					.click();
			Thread.sleep(3000);
			driverAndroid.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[4]"))
					.click();
		}

	}

	/*
	 * 1 - OP��O SITE PORTO SEGURO CLICA NA OP��O INICIAR CHAT(D� ERRO POIS
	 * SOLICITARAM QUE N�O SUJASSE A BASE DO CLIENTE) 2- OP��O APLICATIVO CLICA NA
	 * OP��O INICIAR CHAT(D� ERRO POIS SOLICITARAM QUE N�O SUJASSE A BASE DO
	 * CLIENTE)
	 */

	@SuppressWarnings("static-access")
	@And("^clicar no botao iniciar conversa$")
	public void ClicarBotaoAcessar() throws Throwable {
		// WEB
		if (OpcaoTeste.equals("1")) {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"btnSubmitText\"]")).click();
			Thread.sleep(4000);
			driver.quit();

		}
		// WEB

		// ANDROID
		if (OpcaoTeste.equals("2")) {
			Thread.sleep(2000);
			Scroll.scrollDown();
			Thread.sleep(3000);

			/*
			 * 2- OP��O APLICATIVO PORTO SEGURO USA O TAP POR COORDENADAS POIS POR CLICK()
			 * N�O ESTAVA FUNCIONANDO
			 */
			TouchAction a2 = new TouchAction(driverAndroid);
			a2.tap(373, 1087).perform();
			Thread.sleep(3000);
			/*
			 * 2- OP��O APLICATIVO PORTO SEGURO TIRA PRINT
			 */
			Robot robot;
			try {
				robot = new Robot();
				BufferedImage screenShot = robot
						.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
				ImageIO.write(screenShot, "JPG", new File(
						"C://Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/target/ScreenShot.jpg"));
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// ANDROID

	}

	/*
	 * 
	 * 
	 * 1- OP��O WEB PORTO SEGURO SEGURO DE VIDA
	 * 
	 * @Given("^estar com o sistema aberto$") public void abrirPaginaPorto() throws
	 * Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { System.setProperty("webdriver.chrome.driver",
	 * "C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/chromedriver.exe"
	 * ); driver = new ChromeDriver(); driver.manage().timeouts().implicitlyWait(20,
	 * TimeUnit.SECONDS); driver.manage().window().maximize();
	 * driver.navigate().to("https://www.portoseguro.com.br"); } }
	 * 
	 * 
	 * 
	 * 
	 * 1- OP��O WEB ABRE O SEGURO DE VIDA
	 * 
	 * 
	 * @When("^eu abrir o seguro de vida$") public void AbrirSeguroVida() throws
	 * Throwable { if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * driver.findElement(By.xpath("//*[@id=\"nav-footer\"]/section[1]/ul/li[8]/a"))
	 * .click(); Thread.sleep(2000);
	 * driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/ul/li[1]/a/h3/span"
	 * )).click(); } }
	 * 
	 * 
	 * 1- OP��O WEB ALTERA A GUIA E CLICA EM INICIAR ESTUDO
	 * 
	 * 
	 * @Then("^comecar um estudo da simulacao$") public void ComecarEstudo() throws
	 * Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { for (String WinHalndle :
	 * driver.getWindowHandles()) {
	 * 
	 * driver.switchTo().window(WinHalndle); } driver.getCurrentUrl();
	 * Thread.sleep(2000);
	 * driver.findElement(By.xpath("/html/body/div[1]/div/a")).click();
	 * Thread.sleep(2000); } }
	 * 
	 * 
	 * 1- OP��O WEB INSERE O NOME NO CHAT DE SEGURO DE VIDA
	 * 
	 * 
	 * @When("^informar um nome\"(.*?)\" valido$") public void
	 * InformarNomeValido(String Nome) throws Throwable { if(OpcaoTeste.equals("1"))
	 * { Thread.sleep(2000);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(Nome);
	 * String selectAll = Keys.chord(Keys.ENTER);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(selectAll)
	 * ; Thread.sleep(2000); } }
	 * 
	 * 
	 * 
	 * 
	 * 1- OP��O WEB INSERE A IDADE NO CHAT DE SEGURO DE VIDA
	 * 
	 * @And("^informar uma idade\"(.*?)\" valida$") public void
	 * InformarIdadeValida(String Idade) throws Throwable {
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000); String selectAll2 =
	 * Keys.chord(Keys.ENTER);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(Idade);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(selectAll2
	 * ); Thread.sleep(2000); } }
	 * 
	 * 
	 * 1- OP��O WEB INSERE O GENERO NO CHAT DE SEGURO DE VIDA
	 * 
	 * 
	 * @And("^informar um genero valido$") public void InformarGeneroValido() throws
	 * Throwable { if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * driver.findElement(By.xpath(
	 * "//*[@id=\"web11-chat-app\"]/div/div/div[2]/div[5]/ul/li[1]/label/span")).
	 * click(); Thread.sleep(2000); } }
	 * 
	 * 
	 * 1- OP��O WEB INSERE A PROFISS�O NO CHAT DE SEGURO DE VIDA
	 * 
	 * 
	 * @And("^informar uma profissao\"(.*?)\" valida$") public void
	 * InformarProfissaoValida(String Profissao) throws Throwable {
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000); String selectAll3 =
	 * Keys.chord(Keys.ENTER);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(Profissao)
	 * ;
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(selectAll3
	 * ); Thread.sleep(2000); } }
	 * 
	 * 
	 * 1- OP��O WEB INSERE A OP��O NO CHAT DE SEGURO DE VIDA
	 * 
	 * @And("^informar uma opcao valida$") public void InformarOpcaoValida() throws
	 * Throwable { if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * driver.findElement(By.xpath(
	 * "//*[@id=\"web11-chat-app\"]/div/div/div[2]/div[8]/div/ul/li[2]/label")).
	 * click(); Thread.sleep(2000); } }
	 * 
	 * 
	 * 1- OP��O WEB INSERE A RENDA NO CHAT DE SEGURO DE VIDA
	 * 
	 * @And("^informar uma renda\"(.*?)\" valida$") public void
	 * InformarRendaValida(String Renda) throws Throwable {
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000); String selectAll4 =
	 * Keys.chord(Keys.ENTER);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(Renda);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(selectAll4
	 * ); Thread.sleep(2000); } }
	 * 
	 * 
	 * 1- OP��O WEB INSERE P CPF NO CHAT DE SEGURO DE VIDA
	 * 
	 * 
	 * @SuppressWarnings("static-access")
	 * 
	 * @And("^informar um cpf\"(.*?)\" invalido$") public void
	 * InformarCPFInvalido(String CPF) throws Throwable { if(OpcaoTeste.equals("1"))
	 * { Thread.sleep(2000); String selectAll5 = Keys.chord(Keys.ENTER);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(CPF);
	 * driver.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys(selectAll5
	 * ); Thread.sleep(5000); driver.quit(); } }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 1 - WEB TELA TREATRO
	 * 
	 * 
	 * @Given("^eu estar com o sistema aberto$") public void
	 * abrirPaginaPortoTeatro() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { System.setProperty("webdriver.chrome.driver",
	 * "C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/chromedriver.exe"
	 * ); driver = new ChromeDriver(); driver.manage().timeouts().implicitlyWait(20,
	 * TimeUnit.SECONDS); driver.manage().window().maximize();
	 * driver.navigate().to("https://www.portoseguro.com.br"); } }
	 * 
	 * 
	 * 1 - WEB TELA TREATRO CLICAR EM TEATRO
	 * 
	 * 
	 * @When("^eu clicar em teatro porto seguro$") public void TeatroPorto() throws
	 * Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * driver.findElement(By.xpath("//*[@id=\"nav-footer\"]/section[5]/ul/li[8]/a"))
	 * .click(); Thread.sleep(4000); } }
	 * 
	 * 
	 * 1 - WEB TELA TREATRO VALIDA SE ESTOU NA NOVA PAGINA
	 * 
	 * 
	 * @Then("^devo validar se estou na pagina do teatro$") public void
	 * TeatroPortoValida1() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(4000);
	 * Assert.assertEquals(driver.getCurrentUrl(),
	 * "https://www.portoseguro.com.br/a-porto-seguro/teatro-porto-seguro/sobre-o-teatro-porto-seguro"
	 * );
	 * 
	 * } }
	 * 
	 * 
	 * 1 - WEB TELA TREATRO CLICA EM CONFIRA A PROGRAMA��O
	 * 
	 * 
	 * @When("^eu clicar em confira a programacao$") public void
	 * TeatroPortoConfira() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/a")).click();
	 * 
	 * //DRIVER MUDA DE TELA PRA A ABA ATUAL for(String WinHalndle :
	 * driver.getWindowHandles()) {
	 * 
	 * driver.switchTo().window(WinHalndle); } } }
	 * 
	 * 
	 * 1 - WEB TELA TREATRO VALIDA SE EST� NA PAGINA QUE MOSTRA O TOUR
	 * 
	 * 
	 * @Then("^devo validar se estou na pagina do teatro porto seguro$") public void
	 * TeatroPortoValida2() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * driver.navigate().to("http://www.teatroportoseguro.com.br/");
	 * Thread.sleep(3000); Assert.assertEquals(driver.getCurrentUrl(),
	 * "http://www.teatroportoseguro.com.br/");
	 * 
	 * 
	 * } }
	 * 
	 * 
	 * 
	 * 1 - WEB TELA TREATRO CLICA EM TOUR
	 * 
	 * 
	 * @When("^eu clicar em tour$") public void TeatroPortoClicaTour() throws
	 * Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(4000);
	 * driver.findElement(By.xpath("//*[@id=\"novidades\"]/div[1]/a/img")).click();
	 * //DRIVER MUDA DE TELA PRA A ABA ATUAL Thread.sleep(4000); for(String
	 * WinHalndle : driver.getWindowHandles()) {
	 * 
	 * driver.switchTo().window(WinHalndle); }
	 * 
	 * 
	 * } }
	 * 
	 * 
	 * 1 - WEB TELA TREATRO CLICA EM TOUR
	 * 
	 * 
	 * @Then("^devo validar se estou no tour$") public void TeatroPortoValida5()
	 * throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(2000);
	 * Assert.assertEquals(driver.getCurrentUrl(),
	 * "http://www.teatroportoseguro.com.br/tour360/index.html");
	 * Thread.sleep(4000);
	 * 
	 * } }
	 * 
	 * @When("^eu clicar em iniciar$") public void TeatroPortoClicaIniciar() throws
	 * Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) {
	 * 
	 * 
	 * 
	 * 
	 * 
	 * //BOT�O PARA ENTRAR NO TOUR Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[6]")).click();
	 * 
	 * //BOT�O PARA MAXIMAR O TOUR Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[13]")).click();
	 * Thread.sleep(2000);
	 * 
	 * WebElement Testar=
	 * driver.findElement(By.xpath("//*[@id=\"krpanoSWFObject\"]")); Actions act=new
	 * Actions(driver); act.moveToElement(Testar); act.clickAndHold();
	 * 
	 * act.moveByOffset(4000,4000); act.moveByOffset(10000,9000);
	 * act.moveByOffset(70000,20000); act.moveByOffset(114000,4000);
	 * act.moveByOffset(4000,114000);
	 * 
	 * act.release(); act.build().perform();
	 * 
	 * Thread.sleep(5000);
	 * 
	 * 
	 * //PLAETEIAS Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[19]/div[2]")).click(); //
	 * BALC�O DIREITO Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[30]/div[1]")).click();
	 * 
	 * 
	 * 
	 * //PLAETEIAS Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[19]/div[2]")).click();
	 * 
	 * //FRENTE Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[30]/div[8]")).click();
	 * 
	 * 
	 * 
	 * //PLAETEIAS Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[19]/div[2]")).click();
	 * 
	 * Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"krpanoSWFObject\"]/div[1]/div[2]/div[30]/div[15]")).click();
	 * 
	 * Thread.sleep(2000); WebElement Testa =
	 * driver.findElement(By.xpath("//*[@id=\"krpanoSWFObject\"]")); Actions acti =
	 * new Actions(driver); acti.moveToElement(Testa); acti.clickAndHold();
	 * 
	 * acti.moveByOffset(4000,4000); acti.moveByOffset(10000,9000);
	 * acti.moveByOffset(70000,20000); acti.moveByOffset(114000,4000);
	 * acti.moveByOffset(4000,114000);
	 * 
	 * acti.release(); acti.build().perform();
	 * 
	 * 
	 * Thread.sleep(5000); driver.quit();
	 * 
	 * 
	 * 
	 * 
	 * } }
	 * 
	 * 
	 * 
	 * 
	 * TELA INVESTIDORES
	 * 
	 * 
	 * 
	 * @Given("^eu estar com o sistema porto seguro aberto$") public void
	 * TelaInvestidoresInciar() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { System.setProperty("webdriver.chrome.driver",
	 * "C:/Users/gloureirog/Desktop/Projeto porto/br.com.portoseguro/chromedriver.exe"
	 * ); driver = new ChromeDriver(); driver.manage().timeouts().implicitlyWait(20,
	 * TimeUnit.SECONDS); driver.manage().window().maximize();
	 * driver.navigate().to("https://www.portoseguro.com.br");
	 * 
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * @When("^eu posicionar o mouse em parceiros e acionistas$") public void
	 * TelaInvestidoresPosicionar1() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) {
	 * 
	 * Thread.sleep(2000); Actions Posiciona1=new Actions(driver); WebElement
	 * mouse=driver.findElement(By.xpath("//*[@id=\"header\"]/div/div[4]/div[1]/div"
	 * )); Posiciona1.moveToElement(mouse).build().perform();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * @And("^clicar em investidores$") public void
	 * TelaInvestidoresClicaInvestidores() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) {
	 * 
	 * Thread.sleep(2000); driver.findElement(By.xpath(
	 * "//*[@id=\"header\"]/div/div[4]/div[2]/ul/li[3]/a")).click();
	 * 
	 * //muda de guia for(String WinHalndle : driver.getWindowHandles()) {
	 * 
	 * driver.switchTo().window(WinHalndle); }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * @Then("^devo validar se estou na pagina de investidores$") public void
	 * TelaInvestidoresValidar() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(3000); Assert.assertEquals(true,
	 * driver.findElement(By.xpath("//*[@id=\"header\"]/div/h2/a")).isDisplayed());
	 * 
	 * }
	 * 
	 * }
	 * 
	 * @When("^eu posicionar o mouse em informacoes aos acionistas$") public void
	 * TelaInvestidoresPosiciona2() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(3000); Actions Posiciona2=new
	 * Actions(driver); WebElement
	 * mouse2=driver.findElement(By.xpath("//*[@id=\"menu\"]/ul/li[3]/a"));;
	 * Posiciona2.moveToElement(mouse2).build().perform(); }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * @And("^clicar em cotacoes graficos e estimativas$") public void
	 * TelaInvestidoresCotacoes() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(3000);
	 * driver.findElement(By.xpath("//*[@id=\"menu\"]/ul/li[3]/div/ul/li[6]/a")).
	 * click();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * @Then("^devo validar se estou na pagina de estimativas$") public void
	 * TelaInvestidoresValida2() throws Throwable {
	 * 
	 * if(OpcaoTeste.equals("1")) {
	 * 
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * @When("^eu clicar em graficos$") public void ClicaGraficos() throws
	 * InterruptedException {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(4000);
	 * driver.findElement(By.xpath("//*[@id=\"texto\"]/div[1]/h4")).click();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * @And("^eu clicar em cotacoes$") public void ClicaCotacoes() throws
	 * InterruptedException {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(4000);
	 * driver.findElement(By.xpath("//*[@id=\"texto\"]/div[2]/h4")).click();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * @And("^eu clicar em estimativas$") public void ClicaEstimativas() throws
	 * InterruptedException {
	 * 
	 * if(OpcaoTeste.equals("1")) { Thread.sleep(4000);
	 * driver.findElement(By.xpath("//*[@id=\"texto\"]/div[3]/h4")).click();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * @Then("^devo obter as informacoes na tela$") public void ClicaValida() throws
	 * InterruptedException {
	 * 
	 * if(OpcaoTeste.equals("1")) {
	 * 
	 * driver.quit(); }
	 * 
	 * }
	 */

}