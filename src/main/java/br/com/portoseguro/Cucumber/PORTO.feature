@Consulta 
Feature:  Testar a funcionalidade do chat online do site porto seguro

	Para conhecer o chat online
 	Como um usuario
 	Eu quero localiza-lo na tela
 	De modo que eu consiga abrir um chat com um atendente
 	

  @start 
Scenario Outline: Teste do Chat
	Given que eu estou com o sistema aberto
	When eu abrir o chat online
	Then deverei validar se estou no chat online
	When informar um nome <Nome> valido
	And informar um perfil<perfil> valido
	And  clicar no botao iniciar conversa
	
	
	
	      Examples: 
		| Nome             |     perfil     |
		| "Teste Indra"    | "Não Cliente"  |
		
#@enable
#Scenario Outline:Teste do Seguro de Vida
#	Given estar com o sistema aberto
#	When eu abrir o seguro de vida
#	Then comecar um estudo da simulacao
#	When informar um nome<Nome> valido
#	And informar uma idade<Idade> valida
#	And informar um genero valido
#	And informar uma profissao<Profissao> valida
#	And informar uma opcao valida
#	And informar uma renda<Renda> valida
#	And informar um cpf<CPF> invalido
#	
#	
#	      Examples: 
#		| Nome         |    Idade | Profissao              | Renda    | CPF            |
#		| "Gabriel"    | "18"     | "Garagista, manobrista"|"1.880,00"|"123.456.789-96"|
#
#
#@enable
#Scenario Outline:Teste do teatro porto seguro
#	Given eu estar com o sistema aberto
#	When eu clicar em teatro porto seguro
#	Then devo validar se estou na pagina do teatro
#	When eu clicar em confira a programacao
#	Then devo validar se estou na pagina do teatro porto seguro
#	When eu clicar em tour
#	Then devo validar se estou no tour
#	When eu clicar em iniciar
#	
#	Examples: 
#		| Nome  |
#		|"teste"|
#		
#		
#			
#@enable
#Scenario Outline:Teste da relacao com investidores
#	Given eu estar com o sistema porto seguro aberto
#	When eu posicionar o mouse em parceiros e acionistas
#	And clicar em investidores
#	Then devo validar se estou na pagina de investidores
#	When eu posicionar o mouse em informacoes aos acionistas
#	And clicar em cotacoes graficos e estimativas
#	Then devo validar se estou na pagina de estimativas
#    When eu clicar em graficos
#    And eu clicar em cotacoes
#    And eu clicar em estimativas
#    Then devo obter as informacoes na tela
#
#	
#	Examples: 
#		| Nome  |
#        |"teste"|