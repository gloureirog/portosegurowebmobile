package br.com.portoseguro.Cucumber;


import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class) 
@CucumberOptions(features = "src/main/java/br/com/portoseguro/Cucumber/PORTO.feature", 
plugin = {"json:target/test-report1.json"}, glue = {""}, monochrome = true, dryRun = false) 
public class CucumberExecute 
{
	
	
}
